---
layout : page
title  : Der Verein
---

<div class=wrapper markdown=1>
![Logo des Maschinendeck Trier e.V.]({{ "/assets/images/logo_maschinendeck_neu.svg" | prepend: site.baseurl }}){:width="200"}
{:style="text-align:center; margin: 0 0 4rem 0"}

<section class="columns two">
	<p>
		Der Verein ist dazu da, die grundlegenden Dinge für den Space zu organisieren.
		Eine Mitgliedschaft ist erwünscht, aber nicht erforderlich. Mitglieder helfen
		bei der Finanzierung des Raumes und den Laden am Laufen zu halten.
	</p>
	<p>
		Der Maschinendeck e. V. ist gemeinnützig und eingetragen im Vereinsregister des Amtsgerichts
		Wittlich unter der Vereinsregisternummer 41060.
	</p>
</section>

[werde mitglied](#werde-mitglied){:class="btn green big"}
{:style="text-align: center"}

</div>


<section class="stripe yellow">
<div class=wrapper markdown=1>

## Historie
Das Maschinendeck blickt auf eine bewegte Vergangenheit zurück. Von der
Entstehung aus dem *CCC Trier* über unzählige Umzüge in neue Räumlichkeiten
wurde bis zum heutigen Tag viel gelötet, 3D-gedruckt, gefräst, genäht, gegossen,
gefachsimpelt und Vieles mehr.

Die prägnantesten Etappen haben wir hier mal aufgeführt.
</div>
</section>

{% include history.html %}

<section class="stripe green">
<div class=wrapper markdown=1>
## Werde Mitglied ##

Mitglied im Maschinendeck zu werden ist eigentlich ganz einfach.

Wir haben
* eine Satzung
* einen Mitgliedsantrag
* und eine Beitragsordnung
</div>
</section>