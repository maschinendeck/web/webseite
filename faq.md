---
layout : page
title  : FAQ
toc    : true
---

<div class="wrapper faq" markdown=1>


{% for item in site.data.faq %}

### {{item.question}}
{{item.answer}}

{% endfor %}

</div>

[discord]: https://discord.gg/e5xYxA8
