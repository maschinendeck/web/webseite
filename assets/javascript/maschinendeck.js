import EventList    from "./EventList.js";
import SmoothScroll from "./SmoothScroll.js";

document.addEventListener("DOMContentLoaded", () => {
	new SmoothScroll();
	new EventList("https://nextcloud.maschinendeck.org/rest/", "#eventTemplate");
});