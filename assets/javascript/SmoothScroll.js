import $ from "./Select.js";

// there is a bug in chrome, so this does not actually work there
class SmoothScroll {
	constructor() {
		const elements = document.querySelectorAll("a[href^='#']");	

		for (const element of elements) {
			const target = $(element.getAttribute("href"));
			element.addEventListener("click", event => {
				event.preventDefault();
				target.scrollIntoView({ behavior: "smooth" });
			});
		}
	}
}

export default SmoothScroll;