import $ from "./Select.js";

class EventList {
	url;
	container;
	template;

	constructor(url, template, target = ".event-list-content") {
		this.url       = url;
		this.template  = $(template);
		this.container = $(target);

		if (this.container instanceof Node) {
			this.container.innerHTML = `<span class="spinner"></span>`;
			this.load();
		}
	}

	async load() {
		const result = await fetch(this.url).then(response => response.json());
		if (!("data" in result))
			return;
		this.container.innerHTML = "";
		const dateOptions = { weekday: "short", year: "numeric", month: "long", day: "numeric" };

		for (const event of result.data) {
			const templateInstance            = this.template.content.cloneNode(true);
			const date                        = new Date(event.dtstart);
			const {summary : title, location} = event;

			templateInstance.querySelector(".title").textContent    = title;
			templateInstance.querySelector(".date").textContent     = date.toLocaleDateString("de-DE", dateOptions);
			templateInstance.querySelector(".location").textContent = location;

			this.container.appendChild(templateInstance);
		}
	}
}

export default EventList;